package ku.util;
/**
 * 
 */
import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * 
 * @author Kitipoom Kongpetch
 *
 * @param <T>
 */
public class ArrayIterator<T> implements Iterator<T>{
	private T[ ] array;
	private int cursor=0;
	/**
	 * 
	 * @param a is array input.
	 */
	public ArrayIterator(T[] a){
		this.array = a;
	}
	@Override
	public boolean hasNext() {
		int cc = this.cursor;
		if(cc<this.array.length){
			do{
				if(this.array[cc]!=null){
				    return true;
				}
				else{
					cc +=1;
				}
			}while(cc<this.array.length);
			return false;
		}
		else{
			return false;
		}
	}

	@Override
	public T next() {
		if(hasNext()){
			int n=0;
			for(int i = this.cursor;i<this.array.length;i++){
				if(this.array[i]!=null){
					n=i;
					break;
					
				}
			}
			this.cursor=n+1;
			return 	this.array[n];
		}
		else{
			throw new NoSuchElementException( );
		}
		
	}
	@Override
	public void remove(){
		this.array[this.cursor-1]=null;
		
	}
}
