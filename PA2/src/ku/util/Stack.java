package ku.util;

import java.util.EmptyStackException;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 *@param <T>
 */
public class Stack<T>{
	private T[] items;
	/**
	 * 
	 * @param capacity size of Stack.
	 */
	public Stack(int capacity){
		int a;
		if(capacity>0){
			a=	capacity;	
		}
		else{
			a=	0;	
		}
		this.items= (T[]) new Object[a];
	}
	/**
	 * 
	 * @return maximum number of elements that this Stack can hold.
	 */
	public int capacity( ){
		
		if(this.items.length==Double.POSITIVE_INFINITY){
			return -1;
		}
		return this.items.length;
	}
	/**
	 * 
	 * @return true if stack is empty.
	 */
	public boolean isEmpty( ){
		for(int i=0; i<this.items.length;i++){
			if(this.items[i]!=null){
				return false;
			}
		}
		return true;
	}
	/**
	 * 
	 * @return true if stack is full.
	 */
	public boolean isFull( ){
		for(int i=0; i<this.items.length;i++){
			if(this.items[i]==null){
				return false;
			}
		}
		return true;
	}
	/**
	 * 
	 * @return return the item on the top of the stack, without removing it.  
	 */
	public T peek(){
		if(this.isEmpty()){
			return null;
		}
		else{
			for(int i= this.items.length-1;i>=0;i--){
				if(this.items[i]!=null){
					return this.items[i];
				}
			}
			return null;
		}
	}
	/**
	 * 
	 * @return the item on the top of the stack, and remove it from the stack.
	 */
	public T pop( ){
		if(this.isEmpty()){
			throw new EmptyStackException( );
		}
		else{
			T a;
			for(int i= this.items.length-1;i>=0;i--){
				if(this.items[i]!=null){
					a = this.items[i];
					this.items[i]=null;
					return a;
				}
			}
			throw new EmptyStackException( );
		}
	}
	/**
	 * 
	 * @param obj object we want to add in Stack.
	 */
	public void push( T obj ){
		if(obj==null){
			throw new IllegalArgumentException( );
		}
		else{
			if(!this.isFull()){
				for(int i=0; i<this.items.length;i++){
					if(this.items[i]==null){
						this.items[i]=obj;
						break;
					}
				}
			}
		}
	}
	/**
	 * 
	 * @return the number of items in the stack.
	 */
	public int size( ){
		int a=0;
		for(int i=0; i<this.items.length;i++){
			if(this.items[i]!=null){
				a++;
			}
		}
		return a;
	}		
}
